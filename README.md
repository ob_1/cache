cache
=====

Memory-Hierarchy Understanding Tools

These are a few programs that benchmark different access patterns and graph the results to understand the cache hierarchy of the computer they are run on.
